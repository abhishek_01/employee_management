import React from "react";
import "./Loader.css";

const CircularProgress = () => (
  <div className="loadingio-spinner-eclipse-bfwbiihipu5">
    <div className="ldio-vcz1gpznvy">
      <div></div>
    </div>
  </div>
);

export default CircularProgress;
