import React, { Component } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { addEmployee, updateEmployee } from "../../actions";
import cuid from "cuid";
import { compose } from "redux";

class AddEmployeeComponent extends Component {
  constructor() {
    super();
    this.state = {
      formatedDate: "",
      data: {},
    };
  }
  handleCancel = () => {
    this.props.history.push("/");
  };
  render() {
    console.log(this.props.location.state);
    const phoneRegExp = /^[0-9]{10}$/;
    return (
      <div className="main" style={{ marginLeft: "25%", marginRight: "25%" }}>
        <div className="row">
          <div className="col-12">
            <h2 className="text-center">
              {this.props.location.state
                ? "Update Employee Detail"
                : "Add Employee Detail"}
            </h2>
            <Formik
              initialValues={{
                firstName: this.props.location.state
                  ? this.props.location.state.firstName
                  : "",
                lastName: this.props.location.state
                  ? this.props.location.state.lastName
                  : "",
                email: this.props.location.state
                  ? this.props.location.state.email
                  : "",
                phoneNo: this.props.location.state
                  ? this.props.location.state.phoneNo
                  : "",
                empId: this.props.location.state
                  ? this.props.location.state.empId
                  : "",
                empType: this.props.location.state
                  ? this.props.location.state.empType
                  : "Parmanent",
                id: this.props.location.state
                  ? this.props.location.state.id
                  : cuid(),
              }}
              validationSchema={Yup.object().shape({
                firstName: Yup.string().required("First Name is required"),
                lastName: Yup.string().required("Last Name is required"),
                email: Yup.string()
                  .email("Email is invalid")
                  .required("Email is required"),

                phoneNo: Yup.string()
                  .matches(
                    phoneRegExp,
                    "Phone number is not valid.It should be 10 digits."
                  )
                  .required("Phone Number is required"),
                empId: Yup.string().required("Employee Id is required"),
              })}
              onSubmit={(fields) => {
                console.log("fields", fields);

                this.setState({ data: fields });
                console.log("data", this.state.data);
                if (this.props.location.state) {
                  this.props.editEmp(this.state.data);
                } else {
                  this.props.setEmployeeData({
                    data: this.state.data,
                  });
                }

                this.props.history.push("/");
              }}
              render={({ errors, touched }) => (
                <Form>
                  <div className="form-group">
                    <label htmlFor="firstName">First Name *</label>
                    <Field
                      name="firstName"
                      type="text"
                      className={
                        "form-control" +
                        (errors.firstName && touched.firstName
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="firstName"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="lastName">Last Name *</label>
                    <Field
                      name="lastName"
                      type="text"
                      className={
                        "form-control" +
                        (errors.lastName && touched.lastName
                          ? " is-invalid"
                          : "")
                      }
                    />
                    <ErrorMessage
                      name="lastName"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="email">Email *</label>
                    <Field
                      name="email"
                      type="text"
                      className={
                        "form-control" +
                        (errors.email && touched.email ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="email"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="phoneNo">Phone No *</label>
                    <Field
                      name="phoneNo"
                      type="text"
                      className={
                        "form-control" +
                        (errors.phoneNo && touched.phoneNo ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="phoneNo"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="empId">Employee Id *</label>
                    <Field
                      name="empId"
                      type="text"
                      className={
                        "form-control" +
                        (errors.empId && touched.empId ? " is-invalid" : "")
                      }
                    />
                    <ErrorMessage
                      name="empId"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="empType">Employee Type</label>
                    <Field
                      className={
                        "form-control" +
                        (errors.empType && touched.empType ? " is-invalid" : "")
                      }
                      as="select"
                      name="empType"
                    >
                      {["Parmanent", "Contract"].map((i) => (
                        <option key={i} value={i}>
                          {i}
                        </option>
                      ))}
                    </Field>

                    <ErrorMessage
                      name="empId"
                      component="div"
                      className="invalid-feedback"
                    />
                  </div>
                  <div className="form-group text-center">
                    <button type="submit" className="btn btn-primary mr-2">
                      {this.props.location.state ? "Update" : "Submit"}
                    </button>
                    <button
                      type="reset"
                      className="btn btn-secondary"
                      onClick={this.handleCancel}
                    >
                      cancel
                    </button>
                  </div>
                </Form>
              )}
            />
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({
  setEmployeeData: (payload) => dispatch(addEmployee(payload)),
  editEmp: (payload) => dispatch(updateEmployee(payload)),
});

export default compose(
  withRouter,
  connect(null, mapDispatchToProps)
)(AddEmployeeComponent);
