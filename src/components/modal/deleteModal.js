import React, { Component } from "react";
import { Button, Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { deleteEmployee } from "../../actions";
class DeleteModal extends Component {
  constructor() {
    super();
    this.state = {};
  }
  handleDelete = () => {
    this.props.deleteEmployeeData(this.props.id);
    this.props.handleClose();
  };
  render() {
    return (
      <>
        <Modal show={this.props.show} onHide={this.props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Delete</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Are you sure you want to delete ?
            <div className="mt-3" style={{ fontSize: "14px" }}>
              <strong>This action cannot be undone.</strong>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.handleClose}>
              Close
            </Button>
            <Button variant="danger" onClick={this.handleDelete}>
              Delete
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({
  deleteEmployeeData: (id) => dispatch(deleteEmployee(id)),
});
export default connect(null, mapDispatchToProps)(DeleteModal);
