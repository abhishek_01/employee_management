import React, { Component } from "react";
import { Modal } from "react-bootstrap";
class ViewModal extends Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    return (
      <>
        <Modal show={this.props.show} onHide={this.props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Employee Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">
              <div className="col-4">
                <label>
                  <strong>First Name :</strong>
                </label>
              </div>
              <div className="col-6">
                <p className="d-inline pl-5">
                  {this.props.empData && this.props.empData.firstName}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>
                  <strong>Last Name :</strong>
                </label>
              </div>
              <div className="col-6">
                <p className="d-inline pl-5">
                  {this.props.empData && this.props.empData.lastName}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>
                  <strong>Email :</strong>
                </label>
              </div>
              <div className="col-6">
                <p className="d-inline pl-5">
                  {this.props.empData && this.props.empData.email}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>
                  <strong>Phone No. :</strong>
                </label>
              </div>
              <div className="col-6">
                <p className="d-inline pl-5">
                  {this.props.empData && this.props.empData.phoneNo}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>
                  <strong>Employee Id :</strong>
                </label>
              </div>
              <div className="col-6">
                <p className="d-inline pl-5">
                  {this.props.empData && this.props.empData.empId}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <label>
                  <strong>Employee Type :</strong>
                </label>
              </div>
              <div className="col-6">
                <p className="d-inline pl-5">
                  {this.props.empData && this.props.empData.empType}
                </p>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </>
    );
  }
}
export default ViewModal;
