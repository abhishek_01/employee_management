import React, { Component } from "react";
import { withRouter } from "react-router";
import DeleteModal from "../modal/deleteModal";
import ViewModal from "../modal/viewModal";
import { compose } from "redux";
import { connect } from "react-redux";
import MaterialTable from "material-table";

class HomePageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDeleteModal: false,
      showViewModal: false,
    };
  }
  handleDelete = () => {
    this.setState({ showDeleteModal: true });
  };
  handleEdit = (item) => {
    this.props.history.push({
      pathname: "/employeeData",
      state: item,
    });
  };
  handleView = () => {
    this.setState({ showViewModal: true });
  };
  handleAdd = () => {
    this.props.history.push("/employeeData");
  };
  handleCloseView = () => this.setState({ showViewModal: false });
  handleCloseDelete = () => this.setState({ showDeleteModal: false });

  render() {
    const createRow =
      this.props.employeesData &&
      this.props.employeesData.map((val) => {
        return val;
      });

    return (
      <div className="main">
        <div className="row">
          <div className="col-12">
            <h1 className="same-line">Employee Details</h1>
            <button
              className="btn btn-primary float-right same-line"
              onClick={() => this.handleAdd()}
            >
              Add Employee
            </button>
          </div>
        </div>
        <div className="row mt-2">
          <div className="col-12">
            <MaterialTable
              title="Employee List"
              columns={[
                {
                  title: "First Name",
                  field: "firstName",
                },
                {
                  title: "Last Name",
                  field: "lastName",
                },
                {
                  title: "Email",
                  field: "email",
                },
                {
                  title: "Phone No",
                  field: "phoneNo",
                },
                {
                  title: "Employee Id",
                  field: "empId",
                },
                {
                  title: "Employee Type",
                  field: "empType",
                },
              ]}
              data={createRow}
              actions={[
                {
                  icon: "visibility",
                  tooltip: "View User Data",
                  onClick: (event, rowData) => {
                    this.setState({ viewData: rowData });
                    this.handleView(rowData);
                  },
                },
                {
                  icon: "edit",
                  tooltip: "Update User Data",
                  onClick: (event, rowData) => this.handleEdit(rowData),
                },
                (rowData) => ({
                  icon: "delete",
                  tooltip: "Delete User Data",
                  onClick: (event, rowData) => {
                    this.setState({ deleteId: rowData.id });
                    this.handleDelete();
                  },
                }),
              ]}
              options={{
                actionsColumnIndex: -1,
              }}
            />
          </div>
        </div>
        <ViewModal
          show={this.state.showViewModal}
          empData={this.state.viewData}
          handleClose={this.handleCloseView}
        />
        <DeleteModal
          show={this.state.showDeleteModal}
          id={this.state.deleteId}
          handleClose={this.handleCloseDelete}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  employeesData: state.employeesDetails.data,
});
export default compose(
  withRouter,
  connect(mapStateToProps, null)
)(HomePageComponent);
