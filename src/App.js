import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { hot } from "react-hot-loader";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import Loadable from "react-loadable";
import "jquery";
import "popper.js";
import "bootstrap/dist/js/bootstrap.bundle.min";
// Bootstarap css
import "bootstrap/dist/css/bootstrap.min.css";
//Custome Css
import "./assets/css/styles.css";
// Loader css
import CircularProgress from "./components/Loader/Loader";
import store from "./store";
import { persistor } from "./store";

const loading = () => (
  <>
    <CircularProgress />
  </>
);

const AsyncHome = Loadable({
  loader: () => import("./components/homePage/homePage"),
  loading: loading,
});
const AddEmployee = Loadable({
  loader: () => import("./components/addEmployeeDetail/addEmployeeDetails"),
  loading: loading,
});
class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Router>
            <Switch>
              <Route exact path="/" component={AsyncHome} />
              <Route exact path="/employeeData" component={AddEmployee} />
            </Switch>
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}
export default hot(module)(App);
