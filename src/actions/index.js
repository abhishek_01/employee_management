import { ADD_EMPLOYEE, UPDATE_EMPLOYEE, DELETE_EMPLOYEE } from "./types";

export const addEmployee = (payload) => ({
  type: ADD_EMPLOYEE,
  payload,
});
export const updateEmployee = (payload) => ({
  type: UPDATE_EMPLOYEE,
  payload,
});
export const deleteEmployee = (id) => ({
  type: DELETE_EMPLOYEE,
  payload: id,
});
