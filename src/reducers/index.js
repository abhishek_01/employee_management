import { combineReducers } from "redux";
import employeesDetails from "./employee_slice";
const rootReducer = combineReducers({
  employeesDetails,
});
export default rootReducer;
