const initialState = {
  data: [],
};
const employeesDetails = (state = initialState, action) => {
  console.log("reducer", action);
  switch (action.type) {
    case "ADD_EMPLOYEE":
      return {
        ...state,
        data: [...state.data, action.payload.data],
      };
    case "UPDATE_EMPLOYEE":
      let item_index = "";
      let previous_data = state.data;
      state.data.filter((item, index) => {
        if (item.id == action.payload.id) {
          item_index = index;
        }
      });
      previous_data[item_index] = action.payload;
      return {
        ...state,
        data: previous_data,
      };
    case "DELETE_EMPLOYEE":
      return {
        ...state,
        data: state.data.filter((item) => item.id !== action.payload),
      };
    default:
      return state;
  }
};
export default employeesDetails;
